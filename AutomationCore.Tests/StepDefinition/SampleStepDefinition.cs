﻿namespace AutomationCore.StepDefinition
{
    using ComponentHelper;
    using PageObject;
    using Settings;
    using TechTalk.SpecFlow;

    [Binding]
    public sealed class SampleStepDefinition
    {
        private SamplePageObject sPage;

        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        [Given(@"I have opened ""(.*)"" in a web browser")]
        public void GivenIHaveOpenedInAWebBrowser(string p0)
        {
            NavigationHelper.NavigateToUrl(p0);
            this.sPage = new SamplePageObject(ObjectRepository.Driver);
        }

        [When(@"I enter ""(.*)"" in the Search box")]
        public void WhenIEnterInTheSearchBox(string p0)
        {
            this.sPage.SubmitSearch(p0);
        }

        [Then(@"The first link returned contains the word ""(.*)""")]
        public void ThenTheFirstLinkReturnedContainsTheWord(string p0)
        {
            this.sPage.CheckFirstSearchResult(p0);
        }
    }
}
