﻿namespace AutomationCore.PageObject
{
    using System;
    using ComponentHelper;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class PageBase
    {
        private IWebDriver driver;

        [FindsBy(How = How.LinkText, Using = "Home")]
        private IWebElement homeLink;

        public PageBase(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
            this.driver = driver;
        }

        public void Logout()
        {
            if (GenericHelper.IsElementPresent(By.XPath(" ")))
            {
                ButtonHelper.ClickButton(By.XPath(" "));
            }

            GenericHelper.WaitForWebElementInPage(By.Id(" "), TimeSpan.FromSeconds(30));
        }

        protected void NavigateToHomePage()
        {
            this.homeLink.Click();
        }
    }
}
