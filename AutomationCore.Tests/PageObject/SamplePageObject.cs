﻿namespace AutomationCore.PageObject
{
    using System;
    using System.Drawing.Imaging;
    using System.Reflection;
    using BaseClasses;
    using ComponentHelper;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.Extensions;
    using OpenQA.Selenium.Support.PageObjects;

    public class SamplePageObject : PageBase
    {
        private IWebDriver driver;
        
        [FindsBy(How = How.Id, Using = "lst-ib")]
        private IWebElement searchTextBox;

        [FindsBy(How = How.Name, Using = "btnG")]
        private IWebElement searchButton;

        [FindsBy(How = How.XPath, Using = ".//*[@id='rso']/div[1]/div[1]/div/h3/a")]
        private IWebElement firstResultLink;

        public SamplePageObject(IWebDriver driver)
            : base(driver)
        {
            PageFactory.InitElements(driver, this);
            this.driver = driver;
        }

        public void SubmitSearch(string text)
        {
            Log4NetHelper.Layout = "%level %date{-dd MMM yyyy HH:mm:ss }       -%message%newline";
           // ILog Logger = Log4NetHelper.GetLogger(typeof(SamplePageObject));
            
            try
            {
                this.searchTextBox.Clear();
              //  Logger.Info("Searching for Text");
                this.searchTextBox.SendKeys(text);
                this.searchButton.Click();
            }
            catch (Exception e)
            {
             //   Logger.Error(e.StackTrace);
                throw e;
            }
        }

        public void CheckFirstSearchResult(string expected)
        {
            this.firstResultLink.Text.Contains(expected);
        }
    }
}
