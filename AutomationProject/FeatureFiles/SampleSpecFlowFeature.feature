﻿Feature: SampleSpecFlowFeature
	In order demonstrate SpecFlow
	As an Automation Tester
	I want to search Google for "test" and verify the search results

@Google_Search
Scenario: Search Google for the subject "test"
	Given I have opened "http://google.co.uk" in a web browser
	When I enter "test" in the Search box
	Then The first link returned contains the word "test"
