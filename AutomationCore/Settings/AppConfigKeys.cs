﻿namespace AutomationCore.Settings
{
    public class AppConfigKeys
    {
        public const string Browser = "Browser";
        public const string PageLoadTimeout = "PageLoadTimeout";
        public const string ElementLoadTimeout = "ElementLoadTimeout";
    }
}
