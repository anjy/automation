﻿namespace AutomationCore.Settings
{
    using Interfaces;
    using OpenQA.Selenium;

    public class ObjectRepository
    {
        public static IConfig Config { get; set; }

        public static IWebDriver Driver { get; set; }
    }
}
