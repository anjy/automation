﻿namespace AutomationCore.Configuration
{
    public enum BrowserType
    {
        /// <summary>
        /// Firefox
        /// </summary>
        Firefox,

        /// <summary>
        /// Chrome
        /// </summary>
        Chrome,

        /// <summary>
        /// Internet Explorer
        /// </summary>
        IExplorer,

        /// <summary>
        /// Phantom JS Headless Browser
        /// </summary>
        PhantomJs,

        /// <summary>
        /// Mac OS X running Safari browser in BrowserStack
        /// </summary>
        OsxSafariCloud
    }
}
