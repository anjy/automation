﻿namespace AutomationCore.CustomException
{
    using System;

    public class NoSuitableDriverFound : Exception
    {
        public NoSuitableDriverFound(string msg)
            : base(msg)
        {
        }
    }
}
