﻿namespace AutomationCore.BaseClasses
{
    using System;
    using System.Threading;
    using ComponentHelper;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class PageBase
    {
        private IWebDriver driver;

        [FindsBy(How = How.LinkText, Using = "Home")]
        private IWebElement homeLink;

        [FindsBy(How = How.XPath, Using = "//input[@id='ctl00_ContentPlaceHolder1_UsernameTextBox']")]
        private IWebElement userNameinput;

        [FindsBy(How = How.XPath, Using = "//input[@id='ctl00_ContentPlaceHolder1_PasswordTextBox']")]
        private IWebElement passwordinput;

        [FindsBy(How = How.XPath, Using = "//input[@id='ctl00_ContentPlaceHolder1_SubmitButton']")]
        private IWebElement signIn;

        public PageBase(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
            this.driver = driver;
        }

        public IWebDriver Login(string url, string username, string password)
        {
            NavigationHelper.NavigateToUrl(url);
            this.userNameinput.SendKeys(username);
            this.passwordinput.SendKeys(password);
            this.signIn.Click();
            Thread.Sleep(2000);

            return this.driver;
        }

        public IWebDriver Login(string url)
        {
            NavigationHelper.NavigateToUrl(url);
            this.userNameinput.SendKeys("TEST001");
            this.passwordinput.SendKeys("Password1");
            this.signIn.Click();
            Thread.Sleep(2000);

            return this.driver;
        }

        public void Logout()
        {
            if (GenericHelper.IsElementPresent(By.XPath(" ")))
            {
                ButtonHelper.ClickButton(By.XPath(" "));
            }

            GenericHelper.WaitForWebElementInPage(By.Id(" "), TimeSpan.FromSeconds(30));
        }

        protected void NavigateToHomePage()
        {
            this.homeLink.Click();
        }
    }
}
