﻿namespace AutomationCore.BaseClasses
{
    using System;
    using ComponentHelper;
    using Configuration;
    using CustomException;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.IE;
    using OpenQA.Selenium.PhantomJS;
    using OpenQA.Selenium.Remote;
    using Settings;

    public class BaseClass
    {
        public static void InitWebdriver()
        {
            ObjectRepository.Config = new AppConfigReader();

            switch (ObjectRepository.Config.GetBrowser())
            {
                case BrowserType.Firefox:
                    ObjectRepository.Driver = GetFirefoxDriver();
                    break;

                case BrowserType.Chrome:
                    ObjectRepository.Driver = GetChromeDriver();
                    break;

                case BrowserType.IExplorer:
                    ObjectRepository.Driver = GetIeDriver();
                    break;

                case BrowserType.PhantomJs:
                    ObjectRepository.Driver = GetPhantomJsDriver();
                    break;

                case BrowserType.OsxSafariCloud:
                    ObjectRepository.Driver = GetOsxSafariCloudDriver();
                    break;

                default:
                    throw new NoSuitableDriverFound("Driver not found : " + ObjectRepository.Config.GetBrowser().ToString());
            }

            ObjectRepository.Driver.Manage()
                .Timeouts()
                .SetPageLoadTimeout(TimeSpan.FromSeconds(ObjectRepository.Config.GetPageLoadTimeOut()));
            ObjectRepository.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(ObjectRepository.Config.GetElementLoadTimeOut()));
            BrowserHelper.BrowserMaximize();
        }

        public static void TearDown()
        {
            if (ObjectRepository.Driver != null)
            {
                ObjectRepository.Driver.Close();
                ObjectRepository.Driver.Quit();
            }
        }

        private static FirefoxProfile GetFirefoxOptions()
        {
            FirefoxProfile profile = new FirefoxProfile();
            FirefoxProfileManager manager = new FirefoxProfileManager();
            return profile;
        }

        private static ChromeOptions GetChromeOptions()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("start-maximized", "--use-fake-ui-for-media-stream", "--use-fake-device-for-media-stream");
            return option;
        }

        private static InternetExplorerOptions GetIeOptions()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            options.EnsureCleanSession = true;
            options.ElementScrollBehavior = InternetExplorerElementScrollBehavior.Bottom;
            return options;
        }

        private static PhantomJSOptions GetPhantomJsOptions()
        {
            PhantomJSOptions option = new PhantomJSOptions();
            option.AddAdditionalCapability("handlesAlerts", true);
            return option;
        }

        private static ICapabilities GetOsxSafariCloudOptions()
        {
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browserstack.user", "jennifercross1");
            capability.SetCapability("browserstack.key", "pWSfMqnLCcXhzB5Hpodv");
            capability.SetCapability("browser", "Safari");
            capability.SetCapability("browser_version", "9.0");
            capability.SetCapability("os", "OS X");
            capability.SetCapability("os_version", "El Capitan");
            capability.SetCapability("resolution", "1280x960");
            capability.SetCapability("ignoreProtectedModeSettings", true);
            capability.SetCapability("acceptSslCerts", true);
            capability.SetCapability("browserstack.debug", true);
            capability.SetCapability("browserstack.local", true);
            return capability;
        }

        private static FirefoxDriver GetFirefoxDriver()
        {
            FirefoxDriver driver = new FirefoxDriver(GetFirefoxOptions());
            return driver;
        }

        private static ChromeDriver GetChromeDriver()
        {
            ChromeDriver driver = new ChromeDriver(GetChromeOptions());
            return driver;
        }

        private static InternetExplorerDriver GetIeDriver()
        {
            InternetExplorerDriver driver = new InternetExplorerDriver(GetIeOptions());
            return driver;
        }

        private static PhantomJSDriver GetPhantomJsDriver()
        {
            PhantomJSDriver driver = new PhantomJSDriver(GetPhantomJsOptions());
            return driver;
        }

        private static PhantomJSDriverService GetPhantomJsDriverService()
        {
            PhantomJSDriverService service = PhantomJSDriverService.CreateDefaultService();
            service.LogFile = "TestPhantomJS.log";
            service.HideCommandPromptWindow = false;
            service.LoadImages = true;
            return service;
        }

        private static RemoteWebDriver GetOsxSafariCloudDriver()
        {
            RemoteWebDriver driver = new RemoteWebDriver(new Uri("http://hub.browserstack.com/wd/hub/"), GetOsxSafariCloudOptions());
            return driver;
        }
    }
}
