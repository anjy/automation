﻿namespace AutomationCore.Interfaces
{
    using AutomationCore.Configuration;

    public interface IConfig
    {
        BrowserType? GetBrowser();

        int GetPageLoadTimeOut();

        int GetElementLoadTimeOut();
    }
}
