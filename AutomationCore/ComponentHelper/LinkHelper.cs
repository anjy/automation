﻿namespace AutomationCore.ComponentHelper
{
    using OpenQA.Selenium;

    public class LinkHelper
    {
        private static IWebElement element;

        public static void ClickLink(By locator)
        {
            element = GenericHelper.GetElement(locator);
            element.Click();
        }
    }
}
