﻿namespace AutomationCore.ComponentHelper
{
    using System;
    using NUnit.Framework;

    public class AssertHelper
    {
        public static void AreEqual(string expected, string actual)
        {
            try
            {
                Assert.AreEqual(expected, actual);
            }
            catch (Exception)
            {
                // ignore
            }
        }
    }
}
