﻿namespace AutomationCore.ComponentHelper
{
    using AutomationCore.Settings;

    public class NavigationHelper
    {
        public static void NavigateToUrl(string url)
        {
            ObjectRepository.Driver.Navigate().GoToUrl(url);
        }
    }
}
