﻿namespace AutomationCore.ComponentHelper
{
    using Settings;

    public class WindowHelper
    {
        public static string GetTitle()
        {
            return ObjectRepository.Driver.Title;
        }
    }
}
