﻿namespace AutomationCore.ComponentHelper
{
    using System.Threading;
    using OpenQA.Selenium;
    using Settings;

    public class JavaScriptExecutor
    {
        public static object ExecuteScript(string script)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)ObjectRepository.Driver;
            return executor.ExecuteScript(script);
        }

        public static void ScrollToAndClick(IWebElement element)
        {
            ExecuteScript("window.scrollTo(0," + element.Location.Y + ")");
            Thread.Sleep(300);
            element.Click();
        }

        public static void ScrollToAndClick(By locator)
        {
            IWebElement element = GenericHelper.GetElement(locator);
            ExecuteScript("window.scrollTo(0," + element.Location.Y + ")");
            Thread.Sleep(300);
            element.Click();
        }
    }
}
