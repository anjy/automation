﻿namespace AutomationCore.ComponentHelper
{
    using System;
    using log4net;
    using log4net.Appender;
    using log4net.Config;
    using log4net.Core;
    using log4net.Layout;

    public class Log4NetHelper
    {
        // #region Field
        private static ILog logger;
        private static ILog xmlLogger;
        private static ConsoleAppender consoleAppender;
        private static FileAppender fileAppender;
        private static RollingFileAppender rollingFileAppender;
        private static SmtpAppender smtpAppender;
        private static string layout = "%date{dd-MMM-yyyy-HH:mm:ss} [%class] [%level] [%method] - %message%newline";

        // endregion
        // #region Property
        public static string Layout
        {
            set { layout = value; }
        }

        // #endregion
        // #region Public
        public static ILog GetLogger(Type type)
        {
            if (consoleAppender == null)
            {
                consoleAppender = GetConsoleAppender();
            }

            if (fileAppender == null)
            {
                fileAppender = GetFileAppender();
            }

            if (rollingFileAppender == null)
            {
                rollingFileAppender = GetRollingFileAppender();
            }

            if (smtpAppender == null)
            {
                smtpAppender = GetStmpAppender();
            }

            if (logger != null)
            {
                return logger;
            }

            BasicConfigurator.Configure(consoleAppender, fileAppender, rollingFileAppender, smtpAppender);
            logger = LogManager.GetLogger(type);
            return logger;
        }

        public static ILog GetXmlLogger(Type type)
        {
            if (xmlLogger != null)
            {
                return xmlLogger;
            }

            XmlConfigurator.Configure();
            xmlLogger = LogManager.GetLogger(type);
            return xmlLogger;
        }

        // #endregion
        // #region Private
        private static PatternLayout GetPatternLayout()
        {
            var patterLayout = new PatternLayout()
            {
               ConversionPattern = layout
            };
            patterLayout.ActivateOptions();
            return patterLayout;
        }

        private static ConsoleAppender GetConsoleAppender()
        {
            var consoleAppender = new ConsoleAppender()
            {
                Name = "ConsoleAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.Error
            };
            consoleAppender.ActivateOptions();
            return consoleAppender;
        }

        private static FileAppender GetFileAppender()
        {
            var fileAppender = new FileAppender()
            {
                Name = "FileAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
                AppendToFile = true,
                File = "FileLogger.log",
            };
            fileAppender.ActivateOptions();
            return fileAppender;
        }

        private static RollingFileAppender GetRollingFileAppender()
        {
            var rollingAppender = new RollingFileAppender()
            {
                Name = "Rolling File Appender",
                AppendToFile = true,
                File = "RollingFile.log",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
                MaximumFileSize = "1MB",
                MaxSizeRollBackups = 15 // file1.log,file2.log.....file15.log
            };
            rollingAppender.ActivateOptions();
            return rollingAppender;
        }

        private static SmtpAppender GetStmpAppender()
        {
            var smtpAppender = new SmtpAppender()
            {
                Name = "SmtpAppender",
                To = "test@sky.uk",
                From = "noreply@sky.uk",
                Subject = "Test Error Log",
                SmtpHost = "localhost",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
                BufferSize = 512,
                Port = 20
            };
            smtpAppender.ActivateOptions();
            return smtpAppender;
        }

        // #endregion
    }
}
